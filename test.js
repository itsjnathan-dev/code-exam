const myFuction = require('./index');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;

describe('Tests for findLargestNArray function using array = [3, 2, 1, 5, 6, 4]', function () {
  let array = [3, 2, 1, 5, 6, 4];
  it('should return 6 if n = 1', function () {
    myFuction.findLargestNArray(array, 1).should.equal(6);
  });

  it('should return 5 if n = 2', function () {
    myFuction.findLargestNArray(array, 2).should.equal(5);
  });

  it('should return 4 if n = 3', function () {
    myFuction.findLargestNArray(array, 3).should.equal(4);
  });

  it('should return 3 if n = 4', function () {
    myFuction.findLargestNArray(array, 4).should.equal(3);
  });

  it('should return 2 if n = 5', function () {
    myFuction.findLargestNArray(array, 5).should.equal(2);
  });

  it('should return 1 if n = 6', function () {
    myFuction.findLargestNArray(array, 6).should.equal(1);
  });

  it('expect undefined if n = 7', function () {
    expect(myFuction.findLargestNArray(array, 7)).to.be.an('undefined');
  });

  it('expect undefined if n = 0', function () {
    expect(myFuction.findLargestNArray(array, 0)).to.be.an('undefined');
  });

  it('expect undefined if n = "a"', function () {
    expect(myFuction.findLargestNArray(array, "a")).to.be.an('undefined');
  });

  it('expect undefined if n = undefined', function () {
    expect(myFuction.findLargestNArray(array, undefined)).to.be.an('undefined');
  });

  it('expect undefined if no parameter n', function () {
    expect(myFuction.findLargestNArray(array)).to.be.an('undefined');
  });

  it('expect undefined if no parameter', function () {
    expect(myFuction.findLargestNArray()).to.be.an('undefined');
  });


});

describe('Test for findLargestNArray function using array = [3, 2, 1, 5, 6, "K"]', function () {
  let array = [3, 2, 1, 5, 6, 'k'];
  it('expect undefined', function () {
    expect(myFuction.findLargestNArray(array, 2)).to.be.an('undefined');
  });
});

describe('Test for findLargestNArray function using array = "helloworld"', function () {
  let array = "helloworld";
  it('expect undefined', function () {
    expect(myFuction.findLargestNArray(array, 2)).to.be.an('undefined');
  });
});