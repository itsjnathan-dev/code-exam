
/** Coding Question:  Write a function that would:	return the Nth   largest element from the end in an array, and then provide a set of test cases against that function.   

Note: That is the Nth largest element in the sorted order, not the Nth distinct element.

For Example: For example, given [3,2,1,5,6,4] and n = 2, return 5.
 */

module.exports = {

  findLargestNArray: (array, n) => {
    if (!array || !n) return undefined

    // i used bubble sort , but only array that contain numbers can be sorted
    let isSorted;
    do {
      isSorted = false;
      for (let i = 0; i < array.length - 1; i++) {

        if (isNaN(array[i]) || isNaN(array[i + 1])) return undefined;

        if (array[i] < array[i + 1]) {
          let temp = array[i];
          array[i] = array[i + 1];
          array[i + 1] = temp;
          isSorted = true;
        }
        
      }

    } while (isSorted);

    return array[n - 1];
  }

}
